# Testkort til bruk i bigården
Disse kortene er laget for å passe inn i de små gule permene som fulgte med avlsdronninger. Hver side er 1/3 A4 ark og bør skrives ut på et tykt stivt ark. De orginale kortet er omtrent 170gram per kvadratmeter.

Kortene kan brukes til daglig bigårdsbesøk og har kolonner for alle de ulike tingene man ser etter.

PDFen med testkortet er [testkort.pdf](testkort.pdf) og for å enkeltsidene så finnes de i mappen [sidene enkeltvis](sidene_enkeltvis)

Her er en forhåndsvisning av ene siden på testkortet:
![test](forhåndsvisninger/3.png)

## For å gjøre endringer på kortet
For å endre kortet trenger du et program som kan åpne latexfiler og så åpner du [testkort.tex](testkort.tex) gjør endringene dine og så trenger du lualatex for å lage en PDF ut av det.

### Eksempler på tilpasninger:
* Endre på teksten og forklaringene
* Legge til eller fjerne kolonner
* Lage teksten større eller mindre 
* Lage mer plass i cellene eller lage hver linje høyere
